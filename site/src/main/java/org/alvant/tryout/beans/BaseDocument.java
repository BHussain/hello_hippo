package org.alvant.tryout.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="hello_world:basedocument")
public class BaseDocument extends HippoDocument {

}
